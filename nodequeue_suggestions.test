<?php

/**
 * @file
 * Test case for nodequeue_suggestions module
 */

/**
 * Tests the functionality provided by the nodequeue_suggestions module.
 */
class NodeQueueSuggestionsWebTestCase extends DrupalWebTestCase {

  /**
   * Info for simpletest.module.
   */
  public static function getInfo() {
    return array(
      'name' => 'Nodequeue Suggestions',
      'description' => 'Ensure that the nodequeue suggestions module works as expected.',
      'group' => 'Nodequeue',
    );
  }

  /**
   * Setup for individual tests.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp(array('nodequeue_suggestions'));

    // Creating the needed user.
    $this->privileged_user = $this->drupalCreateUser(
      array(
        'administer nodequeue',
        'manipulate queues',
        'manipulate all queues',
      ));
    $this->drupalLogin($this->privileged_user);

    // Create article A.
    $node = new stdClass();
    $node->title = 'Node A';
    $node->type = 'article';
    $node->status = NODE_PUBLISHED;
    $node->uid = 1;
    node_save($node);
    $this->node_a = $node;

    // Create article B.
    $node = new stdClass();
    $node->title = 'Node B';
    $node->type = 'article';
    $node->status = NODE_PUBLISHED;
    $node->uid = 1;
    node_save($node);
    $this->node_b = $node;

    // Create article C.
    $node = new stdClass();
    $node->title = 'Node C';
    $node->type = 'article';
    $node->status = NODE_PUBLISHED;
    $node->uid = 1;
    node_save($node);
    $this->node_c = $node;

    // Create Nodequeue with suggestions.
    $this->drupalGet('admin/structure/nodequeue/add/nodequeue');
    $edit = array(
      'title' => 'Nodequeue with suggestions',
      'size' => 0,
      'reverse' => 0,
      'name' => 'nodequeue_with_suggestions',
      'types[article]' => 'article',
    );
    $this->drupalPost('admin/structure/nodequeue/add/nodequeue', $edit, t('Submit'));

    // Create main Nodequeue.
    $this->drupalGet('admin/structure/nodequeue/add/nodequeue');
    $edit = array(
      'title' => 'Main Nodequeue',
      'size' => 0,
      'reverse' => 0,
      'name' => 'main_nodequeue',
      'types[article]' => 'article',
      'suggestions_enabled' => TRUE,
      'suggestions_queue' => 'nodequeue_with_suggestions',
    );
    $this->drupalPost('admin/structure/nodequeue/add/nodequeue', $edit, t('Submit'));

    $this->main_queue = nodequeue_load_queue_by_name('main_nodequeue');
    $this->main_subqueue = current(nodequeue_load_subqueues_by_queue($this->main_queue->qid));

    $this->suggestion_queue = nodequeue_load_queue_by_name('nodequeue_with_suggestions');
    $this->suggestion_subqueue = current(nodequeue_load_subqueues_by_queue($this->suggestion_queue->qid));
  }

  /**
   * Tests that the moderate page show the correct queues and content.
   */
  public function testAll() {
    nodequeue_subqueue_add($this->main_queue, $this->main_subqueue, $this->node_a->nid);
    nodequeue_subqueue_add($this->suggestion_queue, $this->suggestion_subqueue, $this->node_b->nid);

    $this->drupalGet('admin/structure/nodequeue/' . $this->main_queue->qid . '/moderate/' . $this->main_subqueue->sqid);
    $this->assertText($this->node_a->title, $this->node_a->title . ' should be found');
    $this->assertText($this->node_b->title, $this->node_b->title . ' should be found');
    $this->assertNoText($this->node_c->title, $this->node_c->title . ' should NOT be found');
  }
}
